'use strict';
const AWS = require('aws-sdk');
const db = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});
//const uuid = require('uuid/v4');
//const {"v4": uuidv4} = require('uuid');

const postsTable = process.env.POSTS_TABLE;

console.log("postsTable: ", postsTable);

function response(statusCode, message){
  console.log("response.statusCode:", statusCode);
  console.log("response.message:", message);

  return {
    statusCode: statusCode,
    body: JSON.stringify(message)
  };
}

function sortByDate(a,b){
  if (a.createdAt > b.createdAt) {
    return -1;
  } 
  return 1;  
}

module.exports.createPost = (event, context, callback) => {
  const requestBody = JSON.parse(event.body);

  if (!requestBody.title || requestBody.title.trim() === '' 
  || !requestBody.body || requestBody.body.trim() === '' ){
    return callback(null, response(400, {error: 'Post must have a title and a body and they must not be empty.'}));
  }

  const post = {
    //id: uuid(),
    id: Date.now().toString(),
    // id: Date.now() + "A",
    createdAt: new Date().toISOString(),
    userId: 1,
    title: requestBody.title,
    body: requestBody.body
  };

  console.log("post:",post);

  return db.put({
    TableName: postsTable,
    Item: post
  }).promise().then(()=>{
    callback(null, response(201, post));
  })
  .catch(err => response(null, response(err.statusCode, err)));
};

module.exports.getAllPosts = (event, context, callback) => {
  return db.scan({
    TableName: postsTable
  }).promise().then(res => {
    console.info("res:", res);
    callback(null, response(200, res.Items.sort(sortByDate)));
  })
  .catch(err => callback(null, response(err.statusCode, err)));
};

module.exports.getPosts = (event, context, callback) => {
  const numberOfPosts = event.pathParameters.number;
  const params = {
    TableName: postsTable,
    Limit: numberOfPosts
  }
  return db.scan(params)
    .promise()
    .then(res => {
      console.info("res:", res);
      callback(null, response(200, res.Items.sort(sortByDate)));
    }).catch(err => callback(null, response(err.statusCode, err)));
};

module.exports.getPost = (event, context, callback) => {
  const id = event.pathParameters.id;
  console.info("id: ", id);
  const params = {
    Key: {
      id: id
    },
    TableName: postsTable
  }
  return db.get(params)
    .promise()
    .then(res => {
      console.log("res:", res);
      if (res.Item) callback(null, response(200, res.Item));
      else callback(null, response(404, {error: 'Post not found'}));
    }).catch(err => callback(null, response(err.statusCode, err)));
};

module.exports.updatePost = (event, context, callback) => {
  const id = event.pathParameters.id;
  const body = JSON.parse(event.body);
  const paramName = body.paramName;
  const paramValue = body.paramValue;

  const params = {
    Key: {
      id: id
    },
    TableName: postsTable,
    ConditionExpression: 'attribute_exists(id)',
    UpdateExpression: 'set ' + paramName + ' = :v',
    ExpressionAttributeValues: {
      ':v': paramValue
    },
    ReturnValue: 'ALL_NEW'
  };

  return db.update(params)
    .promise()
    .then(res => {
      console.info("res:", res);
      callback(null, response(200, res.Attributes));
    })
    .catch(err => callback(null, response(err.statusCode, err)));
};

module.exports.deletePost = (event, context, callback) => {
  const id = event.pathParameters.id;
  const params = {
    Key: {
      id: id
    },
    TableName: postsTable
  };
 
  return db.delete(params)
    .promise()
    .then(() => callback(null, response(200, {message: 'Post deleted successfully'})))
    .catch(err => callback(null, response(err.statusCode, err)));
}